<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_youtubeBg
 * @copyright	Copyright (C) 2005 - 2013 Jan B Mwesigwa
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// no direct access
defined('_JEXEC') or die;
?>
<div id="mod_youtubeBg-controls" class="mod_youtubeBg-controls">
    <a href="#prev" class="control arrow prev"></a>
    <a href="#play" class="control play"></a>
    <a href="#pause" class="control pause"></a>
    <a href="#next" class="control arrow next"></a>
</div>
<div class="clear"></div>
<div id="mod_youtubeBg" class="mod_youtubeBg <?php echo $moduleclass_sfx; ?>">
    <div id="mod_youtubeBg_player" type="text/html" allowfullscreen="1" frameborder="0" src="https://www.youtube.com/embed/<?php echo $params->get('videoId').'/?'.http_build_query($playerVars); ?>"></div>
</div>
