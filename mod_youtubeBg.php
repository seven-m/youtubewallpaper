<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_articles_news
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

define('MOD_YOUTUBEBG_PATH',__FILE__);
define('MOD_YOUTUBEBG_URL',JURI::base().'modules/mod_youtubeBg/');


// get video or playlist id from the url
$uri = & JURI::getInstance($params->get('videoUrl',''));
$user = JFactory::getUser();

$params->set('logged', $user->id ? true : false);
$params->set('baseUrl', JURI::base());
$params->set('moduleUrl', MOD_YOUTUBEBG_URL);
$params->set('listId', $uri->getVar('list') ? $uri->getVar('list') : '');
$params->set('videoId', $uri->getVar('v') ? $uri->getVar('v') : null);
// support for SEF formatted uri
if (!$uri->getVar('v') && !$uri->getVar('list')) {
    $parts = explode('/', $params->get('videoUrl',''));
    $key = array_search('watch', $parts) ? array_search('watch', $parts) : array_search('embed', $parts);
    if ($key !== false) $params->set('videoId', $parts[$key + 1]);
}
if (!$params->get('logged')) {

    // set player vars
    $playerVars = array(
        'wmode'=>'transparent',
        'controls'=> $params->get('controls', 0), 
        'autoplay'=> $params->get('autoplay', 0), 
        'origin'=> JURI::base(), /* creates a bug */
        'enablejsapi'=>1,
        'modestbranding'=> 1,
        'showinfo'=>$params->get('showinfo'),
        'rel'=>$params->get('rel'),
        'loop' => 1,
        'iv_load_policy' => 3,
        'list' => $params->get('listId'),
        'listType' => 'playlist'
    );

    $doc = JFactory::getDocument();
    $doc->addStylesheet(MOD_YOUTUBEBG_URL.'css/default.css');
    $doc->addScript(MOD_YOUTUBEBG_URL.'js/default.js');
    $doc->addScriptDeclaration('
        (function ($, window, document) {
                $(function () {
                    $("#mod_youtubeBg").youtubeBg('.$params->toString().');
                });
            } (this.jQuery, this, this.document));
    ');

    require JModuleHelper::getLayoutPath('mod_youtubeBg', $params->get('layout', 'default'));
}