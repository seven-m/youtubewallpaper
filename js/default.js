(function ($, window, document) {
	$(function () {
        
        var log = {
            debug : false,
            error : function(message) {
                var m = 'YoutubeBg error';
                try {
                  if (window.console && window.console.error){
                    window.console.error([m, message].join(': '));
                  }
                } catch (e) {
                  // no console available
                }
            },
            info : function(message) {
                var m = 'YoutubeBg notice';
                try {    
                    if (this.debug && window.console && window.console.info){
                        window.console.info([m, message].join(': '));
                        //$('body').prepend('<div>'+ [m, message].join(': ') +'</div>');
                    }
                } catch (e) {
                  // no console available
                }
            }
        }
        if (!window['YouTubeIframeAPICallbacks']) {
            window['YouTubeIframeAPICallbacks'] = {
                add : function(fn) {
                    this.stack.push(fn);
                },
                stack : [],
                run : function() {
                    $.each(this.stack, function(){
                        this.call();
                    })
                }
            }
        } 
        
        var redraw = function() {
            $('<style />').appendTo($('head')).remove();
            if (window.opera) {
                var bs = document.body.style;
                bs.position = 'relative';
                bs.scale = 'scale(0.9)';
                setTimeout(function() {
                    bs.position = 'static';
                    bs.scale = 'scale(1)';
                }, 1);
            }
        }
        
        var methods = {
            init : function( options ) { 
                return this.each(function() {
                    var $this = $(this),
                        _this = methods,
                        settings = {
                            dataNameSpace : 'youtubeBg',
                            videoId: '',
                            videoUrl: '',
                            aspectRatio: 9/16,
                            width: '100%',
                            height: '100%',
                            // player vars
                            controls: 0,
                            enablejsapi: 1,
                            controls: 0,
                            autoplay: 1,
                            modestbranding : 1,
                            origin : 'http',
                            showinfo : 0,
                            rel : 0,
                            loop : 1,
                            initialOpacity : 0.2
                        };

                    // plugin not yet initialized?
                    if (typeof $this.data(settings.dataNameSpace) == 'undefined') {   
                        $this.data(settings.dataNameSpace, $.extend(settings, options));
                    } else {
                        log.info('youtubeBg is already initialized');
                        return;
                    }
                    
                    // save container
                    methods.$this = $this;
                    
                    var player,
                        $controls = $('#mod_youtubeBg-controls').hide(),
                        $prev = $('.prev', $controls),
                        $next = $('.next', $controls),
                        $play = $('.play', $controls),
                        $pause = $('.pause', $controls),
                        $player = $('#mod_youtubeBg_player', $this);
                    
                    methods.API = {
                        $controls : $controls,
                        $prev : $prev,
                        $next : $next,
                        $play : $play,
                        $pause : $pause,
                        get : methods.get,
                        $player : $player,
                        isMobile : ('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0)
                    }
                    var API = methods.API;
                    
                    if (options.logged) return;
                    
                    var playerVars = {
                            wmode : 'transparent',
                            controls : 0,
                            autoplay : 1,
                            modestbranding : 1,
                            showinfo : 0,
                            rel : 0,
                            loop : 1,
                            origin : settings.origin,
                            iv_load_policy : 3
                        }
                                        
                    // solution for touch screens
                    if (API.isMobile) {
                        $.extend(playerVars, {
                            controls : 1,
                            autoplay : 0,
                            //list : settings.listId,
                            //listType : 'playlist'
                        });
                        
                        var resize = function($player) {
                            var ar = parseFloat(settings.aspectRatio),
                                w = $player.parent().width(),
                                h = w * ar;
                            $player.css({
                                width: '100%',
                                height: h
                            });
                        }
                        
                        $(window).ready(function() {
                            $('#mod_youtubeBg_player').replaceWith(function() { 
                                return $('<iframe />')
                                    .attr('id', $(this).attr('id'))
                                    .attr('allowfullscreen', 1)
                                    .attr('frameborder', 0)
                                });
                            $player = $('#mod_youtubeBg_player');
                            
                            $player.attr('src', 'https://www.youtube.com/embed/'+settings.videoId+'/?' + $.param(playerVars))
                            $controls.hide();
                            $this.css({
                                position: 'relative',
                                opacity: 1,
                                display: 'none',
                                zIndex: 100
                            });
                            
                            resize($player);
                            
                            $player.css({
                                position: 'relative',
                                opacity: 1
                            });
                            if (settings.autoplay === true || parseInt(settings.autoplay)) { 
                                $this.show(2000);
                                $('body').prepend($this);
                            }
                        });

                        $(window).resize(function(){
                            resize($('#mod_youtubeBg_player'));
                        });
                        
                        return;
                    }
                    // end of mobile solution
                    
                    // fn to resize and center iframe in regard to video aspect ratio
                    var resizeIframe = function(iframe) {
                            // output = array [width, height, left, top]
                            var $el = $(iframe),
                                maxW = parseInt($this.css('max-width')) ? parseInt($this.css('max-width')) : 0,
                                w = $(window).width() < maxW ? $(window).width() : maxW,
                                h = $(window).height(),
                                s = [w, h, 0, 0],
                                ar = parseFloat(settings.aspectRatio);
                            
                            // calculate dimensions and position
                            if (s[0] <= s[1]) {
                                s[0] = s[1] / ar;
                                if (s[0] < w) { 
                                    s[0] = w;
                                    s[1] = s[0] * ar;
                                }
                            } else {
                                s[0] = s[1] / ar;
                                if (s[0] < w) { 
                                    s[0] = w > maxW ? maxW : w;
                                    s[1] = s[0] * ar;
                                }
                            }
                            s[2] = ($(window).width() - s[0]) / 2;
                            s[3] = ($(window).height() - s[1]) / 2;

                            // resize and reposition el
                            $el
                                .attr({
                                    width: s[0],
                                    height: s[1]
                                })
                                .css({
                                    left: s[2],
                                    top: s[3]
                                })
                            redraw();
                            return s;
                        }
                    
                    var loaded = false,
                        iFrameReady = function() {
                            var player = new YT.Player('mod_youtubeBg_player', {
                                    playerVars : playerVars,
                                    events : {
                                        onReady: function(event) { 
                                            //save player
                                            methods.API.player = player;
                                            
                                            log.info('youtube player is ready');
                                            // update reference
                                            $player = $('#mod_youtubeBg_player', $this);
                                            methods.API.$player = $('#mod_youtubeBg_player', $this);
                                            
                                            // do initial resize
                                            resizeIframe(event.target.getIframe());
                                            
                                            // dynamic iFrame resize and reposition
                                            $(window).resize(function() {
                                                resizeIframe(event.target.getIframe());
                                            });
                                            
                                            // load video or playlist
                                            if (settings.listId && settings.listId !== 'null') {
                                                log.info('gonna play playlist id: '+settings.listId);
                                                event.target.loadPlaylist({
                                                    list : settings.listId,
                                                    listType : 'playlist',
                                                    suggestedQuality : 'hd720',
                                                    index : 0
                                                }); 
                                                loaded = true;
                                            } else if (settings.videoId && settings.videoId !== 'null') {
                                                log.info('gonna play video id: '+settings.videoId);
                                                $prev.remove(); $next.remove();
                                                event.target.loadVideoById({
                                                    videoId: settings.videoId,
                                                    suggestedQuality : 'hd720'
                                                }); 
                                                loaded = true;
                                            }
                                            
                                        },
                                        onError : function(event) {
                                            log.error('player error: ' + event.data);
                                            _this._freeze();
                                        },
                                        onStateChange : function(event) {
                                            switch(event.data) {
                                                case -1 :
                                                    log.info('video loaded');
                                                    break;
                                                case YT.PlayerState.ENDED : 
                                                    log.info('ended');
                                                    $this.trigger('onEnded');
                                                    break;
                                                case YT.PlayerState.PLAYING : 
                                                    log.info('playing');
                                                    if (loaded && (settings.autoplay === false || !parseInt(settings.autoplay))) {
                                                        event.target.pauseVideo();
                                                        event.target.seekTo(2, true);
                                                        $this.animate({opacity:1});
                                                        $controls.fadeIn();
                                                        loaded = false;
                                                    } else {
                                                        _this._play();
                                                        $this.trigger('onPlay');
                                                    }
                                                    break;
                                                case YT.PlayerState.PAUSED : 
                                                    log.info('paused');
                                                    _this._pause();
                                                    $this.trigger('onPause');
                                                    break;
                                                case YT.PlayerState.BUFFERING :
                                                    log.info('buffering');
                                                    $this.trigger('onBuffering');
                                                    break;
                                            }
                                        }
                                    }
                                }
                            );
                            
                        // get controls and bind click events
                        $prev.click(function(e){
                            e.preventDefault();
                            $player.animate({opacity:0});
                            player.previousVideo();
                        });
                        $next.click(function(e){
                            e.preventDefault();
                            $player.animate({opacity:0});
                            player.nextVideo();
                        });
                        $play.click(function(e){
                            e.preventDefault();
                            $play.css({opacity:0.2});
                            player.playVideo();
                        });
                        
                        $pause.click(function(e){
                            e.preventDefault();
                            player.pauseVideo();
                        });
                    }
                    
                    window['YouTubeIframeAPICallbacks'].add(iFrameReady);
                    if (!window['onYouTubeIframeAPIReady']) {
                        window['onYouTubeIframeAPIReady'] = function() {
                            window['YouTubeIframeAPICallbacks'].run();
                        }
                        $.getScript('https://www.youtube.com/iframe_api');
                    } 

                    // move player to background 
                    $('body').append($this);
                });
            },
            get : function(param, def, type) {
                var $this = methods.$this,
                    o = methods._getOptions();
                type = type ? type : '';
                switch (type) {
                    case 'int' : 
                        return o[param] ? parseInt(o[param]) : (def ? def : false);
                        break;
                    case 'float' : 
                        return o[param] ? parseFloat(o[param]) : (def ? def : false);
                        break;
                    case 'boolean' :
                        if (typeof o[param] === 'boolean') {
                            return o[param];
                        } else if (typeof o[param] === 'string') {
                            p = o[param].toLowerCase(); 
                            return (p == 'true' || p == '1') ? true : false;
                        } else if (typeof o[param] === 'number') {
                            return o[param] == 0 ? false : true;
                        } else {
                            return (def ? def : false);
                        }
                        break;
                    default : return o[param] ? o[param] : (def ? def : '');
                }
            },
            _play : function() {
                var API = methods.API,
                    $this = methods.$this;
                
                if (API.frozen) return;
                if (API.isMobile) {
                    return;
                }                
                $this.css({backgroundSize:0}).animate({opacity: 1},'slow');
                API.$player.animate({opacity: 1},'slow');
                API.$pause.show();
                API.$play.hide().css({opacity:1});
                API.$controls.fadeIn();                
            },
            _pause : function() {
                var API = methods.API,
                    $this = methods.$this;
                
                if (API.frozen) return;
                
                if (API.isMobile) {
                    return;
                }
                $this.css({backgroundSize: 0});
                API.$player.animate({opacity: API.get('initialOpacity')},'slow');
                API.$play.show();
                API.$pause.hide();
                //API.$controls.fadeIn();
            },
            _freeze : function() {
                var API = methods.API,
                    $this = methods.$this;
                
                API.$player.stop().animate({opacity: 0},'slow', function() { $(this).remove(); });
                API.$controls.fadeOut();
                $this.css({
                    backgroundImage: 'url(' + API.get('baseUrl') + 'images/player_background.jpg)',
                    backgroundSize: 'cover'
                }).animate({opacity: API.get('initialOpacity') })
                API.frozen = true;
            },
            _getOption : function(key) {
                var API = methods.API;
                return API.get(key, null);
            },
            _setOption : function(option, value) {
                var o = methods._getOptions();
                if (typeof o[option] !== 'undefined') o[option] = value;
                return this;
            },
            _getPlayer: function() {
                var API = methods.API;
                return API.player ? API.player : false;
            },
            _getOptions : function() {
                var $this = methods.$this;
                return $this.data('youtubeBg');
            },
            pause : function() {
                return this.each(function() {
                    var player = methods._getPlayer();
                    if (player && player.pauseVideo) player.pauseVideo();
                    methods._pause();
                })
            },
            play : function() {
                return this.each(function() {
                    var player = methods._getPlayer();
                    if (player && player.playVideo) player.playVideo();
                    methods._play();
                });
            },
            disable : function() {
                return this.each(function() {
                    $('#mod_youtubeBg_player').detach();
                });
            },
            replace : function(target) {
                return this.each(function() {
                    $('#mod_youtubeBg_player').prependTo(target ? $(target) : $('body'));
                });
            },
            resize : function() {
                return this.each(function() {
                    var $player = $('#mod_youtubeBg_player'),
                        ar = parseFloat(methods.get('aspectRatio')),
                        w = $player.parent().width(),
                        h = w * ar;
                    $player.css({
                        width: '100%',
                        height: h
                    });
                })
            }
        }
        
        $.fn.youtubeBg = function( method ) {
            var public = 'pause, play, option, disable, replace, resize',
                isPublic = false;
            
            if (typeof method === 'string') {
                var r = new RegExp(method, 'i');
                isPublic = r.test(public);
            }
            
            // Method calling logic
            if (typeof method ==='string' && !isPublic) {
                log.error( method + ' is a protected method on jQuery.youtubeBg' );
            } else if ( methods[method] ) {
                return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply( this, arguments );
            } else if (method === 'option' && arguments.length === 2) {
                return methods._getOption.apply( this, Array.prototype.slice.call( arguments, 1 ) );
            } else if (method === 'option' && arguments.length === 3) {
                return methods._setOption.apply( this, Array.prototype.slice.call( arguments, 1 ) );
            } else {
                log.error( 'Method ' +  method + ' does not exist on jQuery.youtubeBg' );
            } 
        }
        
    });
} (this.jQuery.noConflict(), this, this.document));     